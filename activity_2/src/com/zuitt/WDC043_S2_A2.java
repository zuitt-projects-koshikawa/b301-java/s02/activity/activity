package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class WDC043_S2_A2 {

    public static void main(String[] args){


        int[] numArr = new int[5];

        numArr[0] = 2;
        numArr[1] = 3;
        numArr[2] = 5;
        numArr[3] = 7;
        numArr[4] = 11;

        String outputArr = String.format("The first prime number is: %d", numArr[0]);

        System.out.println(outputArr);

        ArrayList<String> studentsList = new ArrayList<String>();

        studentsList.add("John");
        studentsList.add("Jane");
        studentsList.add("Chloe");
        studentsList.add("Zoe");

        String outputStudentList = String.format("My friends are: %s", studentsList);

        System.out.println(outputStudentList);

        HashMap<String, Integer> inventoryList = new HashMap<String, Integer>();

        inventoryList.put("Toothpaste", 15);
        inventoryList.put("Toothbrush", 20);
        inventoryList.put("Soap", 12);

        String outputInventory = String.format("Our current inventory consist of: %s", inventoryList);

        System.out.println(outputInventory);


    }
}
