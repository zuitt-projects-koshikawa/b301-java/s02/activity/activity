package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {

    public static void main(String[] args) {

        int year;
        String output;

        Scanner input = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");
        year = Integer.parseInt(input.nextLine());

        if (year % 400 == 0) {
            output = String.format("%d is a leap year.", year);
        } else if (year % 100 == 0) {
            output = String.format("%d is NOT a leap year.", year);
        } else if (year % 4 == 0) {
            output = String.format("%d is a leap year.", year);
        } else {
            output = String.format("%d is NOT a leap year.", year);
        }

        System.out.println(output);

    }
}
